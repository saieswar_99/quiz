const startButton = document.getElementById('start-btn')
const nextButton = document.getElementById('next-btn')
const questionContainerElement = document.getElementById('question-container')
const questionElement = document.getElementById('question')
const answerButtonsElement = document.getElementById('answer-buttons')

let shuffledQuestions, currentQuestionIndex

startButton.addEventListener('click', startGame)
nextButton.addEventListener('click', () => {
  currentQuestionIndex++
  setNextQuestion()
})

function startGame() {
  startButton.classList.add('hide')
  shuffledQuestions = questions.sort(() => Math.random() - .5)
  currentQuestionIndex = 0
  questionContainerElement.classList.remove('hide')
  setNextQuestion()
}

function setNextQuestion() {
  resetState()
  showQuestion(shuffledQuestions[currentQuestionIndex])
}

function showQuestion(question) {
  questionElement.innerText = question.question
  question.answers.forEach(answer => {
    const button = document.createElement('button')
    button.innerText = answer.text
    button.classList.add('btn')
    if (answer.correct) {
      button.dataset.correct = answer.correct
    }
    button.addEventListener('click', selectAnswer)
    answerButtonsElement.appendChild(button)
  })
}

function resetState() {
  clearStatusClass(document.body)
  nextButton.classList.add('hide')
  while (answerButtonsElement.firstChild) {
    answerButtonsElement.removeChild(answerButtonsElement.firstChild)
  }
}

function selectAnswer(e) {
  const selectedButton = e.target
  const correct = selectedButton.dataset.correct
  setStatusClass(document.body, correct)
  Array.from(answerButtonsElement.children).forEach(button => {
    setStatusClass(button, button.dataset.correct)
  })
  if (shuffledQuestions.length > currentQuestionIndex + 1) {
    nextButton.classList.remove('hide')
  } else {
    startButton.innerText = 'Restart'
    startButton.classList.remove('hide')
  }
}

function setStatusClass(element, correct) {
  clearStatusClass(element)
  if (correct) {
    element.classList.add('correct')
  } else {
    element.classList.add('wrong')
  }
}

function clearStatusClass(element) {
  element.classList.remove('correct')
  element.classList.remove('wrong')
}

const questions = [
  {
    question: 'Who is CEO of GOOGLE?',
    answers: [
      { text: 'Sundar Pichai', correct: true },
      { text: 'Shantanu Narayen', correct: false },
      { text: 'Jeff Bezos', correct: false },
      { text: 'Satya Nadella', correct: false }
    ]
  },
  {
    question: 'What are the Elon Musk Companies?',
    answers: [
      { text: 'Tesla,SpaceX,OpenAI', correct: true },
      { text: 'Audi,ISRO', correct: false },
      { text: 'Bitcoin', correct: false },
      { text: 'TATA,Range Rover', correct: false }
    ]
  },
  {
    question: 'Who is the finanace Minister of INDIA?',
    answers: [
      { text: 'Narendra Modi', correct: false },
      { text: 'Nirmala Sitharaman', correct: true },
      { text: 'Venkayya Naidu', correct: false },
      { text: 'None Of The Above', correct: false }
    ]
  },
  {
    question: 'How Many Alphabets in ENGLISH?',
    answers: [
      { text: '25', correct: false },
      { text: '26', correct: true },
      { text: '27', correct: false },
      { text: '28', correct: false }
    ]
  },
  {
    question: 'What is the Capital Of INDIA?',
    answers: [
      { text: 'New Delhi', correct: true },
      { text: 'Mumbai', correct: false },
      { text: 'Chennai', correct: false },
      { text: 'Kolkata', correct: false }
    ]
  },
  {
    question: 'What is the TESLA Share Price?',
    answers: [
      { text: '985 USD', correct: false },
      { text: '678 USD', correct: false },
      { text: '787 USD', correct: true },
      { text: '887 USD', correct: false }
    ]
  },
  {
    question: 'Who is the Pedal to the medal in World?',
    answers: [
      { text: 'Bernard Arnault', correct: false },
      { text: 'Bill Gates', correct: false },
      { text: 'Jeff Bezos', correct: false },
      { text: 'Elon Musk', correct: true }
    ]
  },
  {
    question: 'What is the capital of Andhra Pradesh?',
    answers: [
      { text: 'Hyderabad', correct: false },
      { text: 'Rajamundry', correct: false },
      { text: 'Vijayawada', correct: false },
      { text: 'Amaravathi', correct: true }
    ]
  },
  {
    question: 'In Which Year did Trump Got Elected as President?',
    answers: [
      { text: '2020', correct: false },
      { text: '2019', correct: false },
      { text: '2018', correct: false },
      { text: '2017', correct: true }
    ]
  }
]